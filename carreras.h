#ifndef CARRERAS_H
#define CARRERAS_H

typedef struct {
	char nem[4], rank[4], leng[4], mat[4], hist[4], cs[4];
} ponderaciones; 

typedef struct {
	char pond[7], psu[7], max[7],min[7];	
} puntaje;

typedef struct {
	char psu[12], bea[22];
} cupos;

typedef struct{
	char facultad[1500];
	char nombre[150];
	char codigo[60];
	ponderaciones pond_e;
	puntaje punt_e;
	cupos cu_o;
	//float prom;
} carrera;


//void cargarEstudiantes(char path[], estudiante curso[]); 

//void grabarEstudiantes(char path[], estudiante curso[]);
void cargarCarreras(char path[], carrera univer[]);
	

#endif //ESTUDIANTES_H

