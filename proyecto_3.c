///////////////////////
#include <stdio.h>   // Aqui se incluyen las librerias readas y/o las que usaremos dentro de nuestro codigo base
#include "carreras.h"
#include <string.h>//
///////////////////////
//interfaz para el usuario pueda ver las opciones a elegir
//Funciones modificadas las cuales seran utilizadas para poder satisfacer lo que se esta pidiendo dentro del codigo
void Consultar_Po_Ca(carrera univer[]){// EN ESTA FUNCION EL USUARIO INGRESARA EL CODIGO DE LA CARRERA, LO CUAL AL HABER INGRESADO EL CODIGO DE LA CARRERA, ESTA FUNCIO LE MOSTRARARA LA PONDERACION QUE NECESITA, Y TODOS LOS DATOS DE LA CARRERA, NEM, RANKING, ULTIMO INGRESADO, ETC.
	int i=0;
	char var[60];
	printf("Digite el  codigo  de carrera que desea consultar\n");
	scanf("%s",var);
	//printf("%s\n",var);
	for(i;i<53;i++){
		//printf("codigo  %d : %s\n",i,univer[i].codigo);
		if (atoi(univer[i].codigo)==atoi(var)){//USAMOS ATO PARA CONVERTIR EL STRING A ENTERO PARA ASI PODER COMPARA EL CODIGO INGRESADO CON CADA CODIGO DE CADA CARRERA QUE SE ENCUENTRA DENTRO DEL TXT.
			printf("EL codigo ingresado es: %s\n",univer[i].codigo);//AL ENCONTRAR EL MISMO COFIGO ESTE LE PROPORCIONARA TODA LA INFORMACION DE LA CARREA, YA SE EL NOMBRE, PONDERACIONES Y VACANTES
			printf("Carrera correspondiente: %s\n",univer[i].nombre);
			printf("Las ponderaciones son Nem: %s Rank: %s Leng: %s Mate: %s Hist: %s ciencia: %s\n",univer[i].pond_e.nem,univer[i].pond_e.rank,univer[i].pond_e.leng,univer[i].pond_e.mat,univer[i].pond_e.hist,univer[i].pond_e.cs);
			printf("Ultimo ingresado: %s\n",univer[i].punt_e.min );
			printf("Cupos por psu: %s , Cupos por BEA: %s\n",univer[i].cu_o.psu,univer[i].cu_o.bea);
		}
	}
	
}
//EN LA SIGUIENTE FUNCION EL USUARIO INGRESARA SUSU SUPUESTOS PUNTAJES PARA PODER VER UNA SIMULACION SI ES FACTIBLE CON LA CARRERA QUE EL QUIERE O LE FALTA PUNTAJE PARA PODER ENTRAR
void Sim_Po_Ca(carrera univer[]){
	int nem_2,rank_2,len_2,mat_2,hist_2,cs_2,cod,i,ponf,dif,leng_mat;//SE CREAN ESTAS VARIABLE PARA PODER GUARDAR NUEVOS PUNTAJES, PARA LUEGO PODER CALCULAR CADA PONDERACION QUE SE REQUIERE PARA LA CARRERA ESCOGIDA
	printf("Ingrese sus puntajes simulados\n");
	printf("Ingrese su nem\n");
	scanf("%d",&nem_2);	
	printf("Ingrese su rank\n");
	scanf("%d",&rank_2);
	printf("Ingrese su puntaje de lenguaje\n");
	scanf("%d",&len_2);
	printf("Ingrese su puntaje de matematicas\n");
	scanf("%d",&mat_2);
	printf("Ingrese su puntaje de historia\n");
	scanf("%d",&hist_2);
	printf("Ingrese su puntaje de ciencias\n");
	scanf("%d",&cs_2);
	printf("Ingrese codigo de carrera\n");
	scanf("%d",&cod);
	for(i=0;i<53;i++){//SE RECORRE COMPLETAMENTE LA LISTA DEL TXT PARA PODER ENCONTRAR LA CARRERA INGRESADA
		if (atoi(univer[i].codigo)==cod){
			if ((atoi(univer[i].pond_e.hist)==0)){//SI INGRESO UNA CARRERA EN AL CUAL NO SE PIDE HISTORIA, EL USUARIO AL HABER INGRESADO CUALQUIER VALOR DENTRO DE LA PREGUNTA DE PUNTAJE DE HISTORIA, ESTE NO SERA TOMADO Y SOLO SERA SACADA CON LOS PUNTAJES QUE EXIJE LA CARRERA
				ponf=((atoi(univer[i].pond_e.nem)*nem_2)/100)+((atoi(univer[i].pond_e.rank)*rank_2)/100)+((atoi(univer[i].pond_e.leng)*len_2)/100)+((atoi(univer[i].pond_e.mat)*mat_2)/100)+((atoi(univer[i].pond_e.cs)*cs_2)/100);//EN ESTA PARTE SE CALCULA LA PONDERACION TOTAL QUE SACARA EL USARIO Y VER SI PUEDE O NO ENTRAR A LA CARRERA CON LOS PUNTAJES INGRESADOS
				dif=ponf-atoi(univer[i].punt_e.min);//ESTA ES LA DIFERENCIA PARA VER SI ESTE TIENE OPORTUNIDADES DE SER UNO DE LOS VACANTES O SIMPLEMENTE AVISAR CUANTO PUNTAJE LE FALTA PONDERAR PARA INGRESAR A LA CARRERA
				if(atoi(univer[i].punt_e.pond)>dif)printf("La ponderacion no es la necesaria para postular  a  la carrera\n");
				printf("La ponderacion de nem para esta carrera es de: %s\n",univer[i].pond_e.nem);
				printf("La ponderacion de ranking de esta carrera es de: %s\n",univer[i].pond_e.rank);
				printf("La ponderacion de lenguaje de esta carrera es de: %s\n",univer[i].pond_e.leng);
				printf("La ponderacion de matematicas de esta carrera es de: %s\n",univer[i].pond_e.mat);
				printf("La ponderacion de historia de esta carrera es de: %s\n",univer[i].pond_e.hist);
				printf("La ponderacion de ciencias de esta carrera es de: %s\n",univer[i].pond_e.cs);
				printf("La poderacion de la carrera es de: %s\n",univer[i].punt_e.min);
				printf("Su ponderacion es de %d\n",ponf);
				printf("El numero de vacantes por psu es de:%s\n ",univer[i].cu_o.psu);
				printf("El numero de vacantes por bea es de:%s\n ",univer[i].cu_o.bea);
			}
			if(atoi(univer[i].pond_e.cs)==0){//SI INGRESO  UNA CARRERA QUE NO EXIGE CIENCIAS, ESTA SOLO TOMARA LOS VALORES QUE SE NECESITAN PARA LA CARRERA INGRESADA
				ponf=((atoi(univer[i].pond_e.nem)*nem_2)/100)+((atoi(univer[i].pond_e.rank)*rank_2)/100)+((atoi(univer[i].pond_e.leng)*len_2)/100)+((atoi(univer[i].pond_e.mat)*mat_2)/100)+((atoi(univer[i].pond_e.hist)*hist_2)/100);
				dif=ponf-atoi(univer[i].punt_e.min);
				printf("La ponderacion de nem para esta carrera es de: %s\n",univer[i].pond_e.nem);
				printf("La ponderacion de ranking de esta carrera es de: %s\n",univer[i].pond_e.rank);
				printf("La ponderacion de lenguaje de esta carrera es de: %s\n",univer[i].pond_e.leng);
				printf("La ponderacion de matematicas de esta carrera es de: %s\n",univer[i].pond_e.mat);
				printf("La ponderacion de historia de esta carrera es de: %s\n",univer[i].pond_e.hist);
				printf("La ponderacion de ciencias de esta carrera es de: %s\n",univer[i].pond_e.cs);
				printf("La poderacion de la carrera es de: %s\n",univer[i].punt_e.min);
				printf("Su ponderacion es de: %d\n",ponf);
				printf("El numero de vacantes por psu es de: %s\n ",univer[i].cu_o.psu);
				printf("El numero de vacantes por bea es de: %s\n ",univer[i].cu_o.bea);
			}
			else{
				//SI LA PONDERACION DE HISTORIA Y CIENCIA SON IGUALES, SE TOMARA CUALQUIERA DE LOS DOS CASOS
				//EN ESTE IF SE VE SI ESQUE LA CARRERA EXIGE CUALQUIERA DE LOS DOS PUNTAJES YA SEA CIENCIAS O HISTORIA, PERO HSITORIA ES MAYOR QUE EL PUNTAJE DE HHISTORIA, ESTE SERA TOMADO CON HISTORIA Y NO CON CIENCIAS, POR LO QUE LA PONDERACION QUEDA CON EL PUNTAJE MAS ALTO OBTENIDO
				if(cs_2<hist_2)ponf=((atoi(univer[i].pond_e.nem)*nem_2)/100)+((atoi(univer[i].pond_e.rank)*rank_2)/100)+((atoi(univer[i].pond_e.leng)*len_2)/100)+((atoi(univer[i].pond_e.mat)*mat_2)/100)+((atoi(univer[i].pond_e.hist)*hist_2)/100);
				//EN ESTE ELSE AL NO CUMPLIR LO DE ARRIBA SE HARA EXACTAMENTE LO MISMO PERO CON CIENCIAS, POR LO QUE SE PONDERARA CON EL PUNTAJE MAS ALTO
				else{ponf=((atoi(univer[i].pond_e.nem)*nem_2)/100)+((atoi(univer[i].pond_e.rank)*rank_2)/100)+((atoi(univer[i].pond_e.leng)*len_2)/100)+((atoi(univer[i].pond_e.mat)*mat_2)/100)+((atoi(univer[i].pond_e.cs)*cs_2)/100);}
				dif=ponf-atoi(univer[i].punt_e.min);
				if(atoi(univer[i].punt_e.pond)>dif)printf("La ponderacion no es la necesaria para postular  a  la carrera\n");
				printf("La diferencia con el ultimo postulante es: %d\n",dif);
				printf("La ponderacion de nem para esta carrera es de: %s\n",univer[i].pond_e.nem);
				printf("La ponderacion de ranking de esta carrera es de: %s\n",univer[i].pond_e.rank);
				printf("La ponderacion de lenguaje de esta carrera es de: %s\n",univer[i].pond_e.leng);
				printf("La ponderacion de matematicas de esta carrera es de: %s\n",univer[i].pond_e.mat);
				printf("La ponderacion de historia de esta carrera es de: %s\n",univer[i].pond_e.hist);
				printf("La ponderacion de ciencias de esta carrera es de: %s\n",univer[i].pond_e.cs);
				printf("La poderacion de la carrera es de: %s\n",univer[i].punt_e.min);
				printf("Su ponderacion es de: %d\n",ponf);
				printf("El numero de vacantes por psu es de: %s\n ",univer[i].cu_o.psu);
				printf("El numero de vacantes por bea es de: %s\n ",univer[i].cu_o.bea);
			}
		}
	}
}
char buscar_fac(int x){
	char cosa[1500];
	printf("entro funcion\n");
	if (x==1)strcpy(cosa,"Facultad_de_Arquitectura");


	if (x==2)strcpy(cosa,"Facultad_de_Ciencias");
	if (x==3)strcpy(cosa,"Facultad_de_Ciencias_del_Mar_y_de_Recursos_Naturales");
	if (x==4)strcpy(cosa,"Facultad_de_Ciencias_Economicas_y_Administrativas");
	if (x==5)strcpy(cosa,"Facultad_de_Derecho_y_Ciencias_Sociales");
	if (x==6)strcpy(cosa,"Facultad_de_Farmacia");
	if (x==7)strcpy(cosa,"Facultad_de_Humanidades");
	if (x==8)strcpy(cosa,"Facultad_de_Ingenieria");
	else{
		strcpy(cosa,"Facultad_de_Medicina");
		}
	//printf("%s\n",cosa );
	return char cosa;
}
void Mostrar_Pond_Facul(carrera univer[]){//EN ESTA FUNCION EL USUARIO INGRESARA UNA DE LAS 7 FACULTADES QUE ESTAN EN EL TXT Y ESTA FUNCION MOSTRARA TODAS LAS CARRERAS PERTENECIENTES A ELLA
	int i,facu;
	char facul[1500];
	printf("digite una de las facultades==>\n (1)Facultad de arquitectura\n (2)Facultad  de  ciencias\n(3)Facultad de Ciencias del mar Y recursos naturales\n(4)Facultad de Ciencias Economicas y Administrativas\n(5)Facultad de derecho y ciencias sociales\n(6)Facultad de Farmacia\n(7)Facultad de Humanidades\n(8)Facultad de Ingenieria\n(9)Facultad_de_Medicina\n");
	scanf("%d",&facu);
	strcpy(facul,buscar_fac(facu));
	printf("%s\n",facul);
	for (i=0;i<53;i++){
		if (strcmp(univer[i].facultad,facul)==0 ){
		printf("\n***%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s\n", univer[i].facultad,univer[i].nombre,univer[i].codigo,univer[i].pond_e.nem,univer[i].pond_e.rank,univer[i].pond_e.leng,univer[i].pond_e.mat,univer[i].pond_e.hist,univer[i].pond_e.cs,univer[i].punt_e.pond,univer[i].punt_e.psu,univer[i].punt_e.max,univer[i].punt_e.min,univer[i].cu_o.psu,univer[i].cu_o.bea);
		//printf("entro  ultimo\n");
		}
	}
		
}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//CREACION DEL MENU DE NUESTRO CODIGO A UTILIZAR, DONDE SE MOSTRARAN LAS OPCIONES QUE PUEDE ESCOGER EL USUARIO
void menu(carrera univer[]){
	int opcion;
    do{
		printf( "\n   1. Consultar ponderacion de notas" );
		printf( "\n   2. Simular Postulacion Carrera" );
		printf( "\n   3. Mostrar Ponderaciones  Facultad" );
		printf( "\n   4. Salir." );
		printf( "\n\n   Introduzca opción (1-4): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: Consultar_Po_Ca(univer); //carga en lote una lista en un arreglo de registros
					break;

			case 2: Sim_Po_Ca(univer);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: Mostrar_Pond_Facul(univer); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					printf("Entro 3\n");
					break;
					}
    } while ( opcion != 4 );
}
//LUGAR DONDE NUESTRO CODIGO SE EJECUTARA Y MOSTRAR EN PANTALLA
int main(){
	carrera univer[53];//ESTA ES EL ARCHIVO DE CARRERAS QUE SE ENCUENTRAN EN EL ARCHIVO TXT QUE HEMOS CREADO, CON TODOS LOS DATOS QUE SE NECESITAN PARA INGRESAR A LA CARRERA  
	cargarCarreras("carrerasUV2018.txt",univer);//AQUI ES DONDE SE CARGAR EL ARCHIVO TXT DE LAS CARRERAS PARA ASI PODER TRABAJAR CON ELLO EN TODO EL CODIGO Y SATISFACER LO PEDIDO 
	menu(univer);//SE EJECUTA EL MENU Y COMIENZA EL CODIGO
	return 0; 
}